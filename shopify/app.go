package shopify

import (
    "gitlab.com/nbminh/shopify_go"
)

func NewShopifyApp(redirectUrl string) *go_shopify.App {
    return &go_shopify.App{
        ApiKey:      "3b58f47a9c98e60a3dcd98c478dfc562",
        ApiSecret:   "23f85def6d16ef2512dd94a0c69359bd",
        RedirectUrl: redirectUrl,
        Scope:       "write_products,write_orders,write_customers,write_draft_orders,write_checkouts,write_price_rules,write_discounts,write_inventory,read_locations,write_assigned_fulfillment_orders,write_fulfillments,read_product_listings,read_all_orders",
    }
}
