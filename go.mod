module gitlab.com/nbminh/hermes

go 1.17

require (
	github.com/shopspring/decimal v1.3.1
	github.com/spf13/cobra v1.6.1
	gitlab.com/minh2101/shopify_proto v0.0.0-20230117024821-7d94ef3923c0
	gitlab.com/nbminh/shopify_go v0.0.0-20230115085604-ab5130cb3781
	gitlab.com/nbminh/shopify_models v0.0.0-20230104025236-3d854bf7e92f
	google.golang.org/grpc v1.51.0
	google.golang.org/protobuf v1.28.1
	gorm.io/driver/mysql v1.4.4
	gorm.io/gorm v1.24.3
)

require (
	github.com/go-sql-driver/mysql v1.6.0 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/grpc-ecosystem/grpc-gateway v1.16.0 // indirect
	github.com/inconshreveable/mousetrap v1.0.1 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	golang.org/x/net v0.0.0-20221014081412-f15817d10f9b // indirect
	golang.org/x/sys v0.0.0-20220908164124-27713097b956 // indirect
	golang.org/x/text v0.4.0 // indirect
	google.golang.org/genproto v0.0.0-20221207170731-23e4bf6bdc37 // indirect
)

//
//replace gitlab.com/nbminh/shopify_go => ../shopify_go
//
//replace gitlab.com/minh2101/shopify_proto => ../shopify_proto
