FROM centos:7

EXPOSE 9090

ADD ./ /usr/bin/hermes

CMD ["hermes", "grpc"]
