package product

import (
    "fmt"

    cpb "gitlab.com/minh2101/shopify_proto/product"
    go_shopify "gitlab.com/nbminh/shopify_go"
)

func PrepareProductDataToResponse(item *go_shopify.Product) *cpb.Product {
    sku := ""
    price := ""
    id := uint64(item.ID)
    if len(item.Variants) > 0 {
        sku = item.Variants[0].Sku
        price = item.Variants[0].Price
    }
    return &cpb.Product{
        Id:     fmt.Sprintf("%d", id),
        Name:   item.Title,
        Sku:    sku,
        Price:  price,
        Type:   item.ProductType,
        Status: item.Status,
    }
}
