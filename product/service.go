package product

import (
    "context"

    pb "gitlab.com/minh2101/shopify_proto/platform"
    cpb "gitlab.com/minh2101/shopify_proto/product"
    product_db "gitlab.com/nbminh/hermes/product_db"
    go_shopify "gitlab.com/nbminh/shopify_go"
)

type Service struct {
    app *go_shopify.App
}

func NewService(app *go_shopify.App) *Service {
    return &Service{app: app}
}

func (s *Service) GetList(ctx context.Context, r *pb.ListProductRequest) (*cpb.ListProductResponse, error) {
    items, _ := s.app.NewClient().Product.List(nil)

    product_db.CreateProductDB(items)

    if nil == items || 0 == len(items) {
        return &cpb.ListProductResponse{}, nil
    }
    list := make([]*cpb.Product, len(items))
    for i, v := range items {
        preparedItem := PrepareProductDataToResponse(&v)
        list[i] = preparedItem
    }
    res := &cpb.ListProductResponse{
        Items: list,
    }
    return res, nil
}
