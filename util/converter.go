package util

import (
    "github.com/shopspring/decimal"
    "strconv"
)

func StringToInt64(value string) int64 {
    ID, _ := strconv.Atoi(value)
    return int64(ID)
}

func StringToDecimal(value string) decimal.Decimal {
    decimalValue, _ := decimal.NewFromString(value)

    return decimalValue
}
