package util

import "github.com/shopspring/decimal"

func IsValidNumber(value string) bool {

	_, err := decimal.NewFromString(value)
	if nil != err {
		return false
	}

	return "" != value && "0" != value && "0.00" != value && "NaN" != value && "Infinity" != value
}
