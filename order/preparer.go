package order

import (
    "fmt"
    "google.golang.org/protobuf/types/known/timestamppb"

    cpb "gitlab.com/minh2101/shopify_proto/order"
    go_shopify "gitlab.com/nbminh/shopify_go"
)

func prepareOrderToResponse(ord *go_shopify.Order) *cpb.Order {
    o := &cpb.Order{
        Id:                  fmt.Sprintf("%d", ord.ID),
        Name:                ord.Name,
        Number:              fmt.Sprintf("%d", ord.Number),
        OrderNumber:         fmt.Sprintf("%d", ord.OrderNumber),
        Currency:            ord.Currency,
        Status:              ord.FinancialStatus,
        PresentmentCurrency: ord.PresentmentCurrency,
        SubtotalPrice:       ord.SubtotalPrice.String(),
        TotalPrice:          ord.TotalPrice.String(),
        TotalTax:            ord.TotalTax.String(),
        SubTotalDiscounts:   ord.TotalDiscounts.String(),
        TotalDiscounts:      ord.TotalDiscounts.String(),
        FulfillmentStatus:   ord.FulfillmentStatus,
        Email:               ord.Email,
        Note:                ord.Note,
        Source:              ord.SourceName,
        OrderStatusUrl:      ord.OrderStatusUrl,
        LocationId:          fmt.Sprintf("%d", ord.LocationId),
        TaxesIncluded:       ord.TaxesIncluded,
        Phone:               ord.Phone,
        Tags:                ord.Tags,
        Items:               PrepareOrderItem(ord.LineItems),
        TaxLines:            PrepareOrderTaxLines(ord.TaxLines),
        Transactions:        PrepareOrderTransactions(ord.Transactions),
        Customer:            PrepareOrderCustomer(ord.Customer),
        BillingAddress:      PrepareAddress(ord.BillingAddress),
        ShippingAddress:     PrepareAddress(ord.ShippingAddress),
        Refunds:             PrepareOrderRefund(ord.Refunds),
        CreatedAt:           timestamppb.New(*ord.CreatedAt),
        UpdatedAt:           timestamppb.New(*ord.UpdatedAt),
    }
    return o
}
