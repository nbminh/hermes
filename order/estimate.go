package order

import (
    "gitlab.com/nbminh/hermes/util"

    pb "gitlab.com/minh2101/shopify_proto/platform"
    go_shopify "gitlab.com/nbminh/shopify_go"
)

func buildRefundEstimateToCreate(r *pb.RefundRequest) go_shopify.Refund {
    refundItems := prepareRefundItemsToCreate(r)

    refund := go_shopify.Refund{
        OrderID: int(util.StringToInt64(r.GetId())),
        Shipping: go_shopify.RefundShipping{
            FullRefund: true,
        },
        RefundLineItems: refundItems,
    }

    return refund
}
