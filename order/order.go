package order

import (
    "github.com/shopspring/decimal"
    "gitlab.com/nbminh/hermes/util"

    cpb "gitlab.com/minh2101/shopify_proto/order"
    tpb "gitlab.com/minh2101/shopify_proto/tax"
    go_shopify "gitlab.com/nbminh/shopify_go"
)

func buildOrderToCreate(o *cpb.Order) go_shopify.Order {
    data := go_shopify.Order{
        LocationId: util.StringToInt64(o.GetLocationId()),
        LineItems:  buildOrderItemsToCreate(o.GetItems()),
        Currency:   o.GetCurrency(),
        Customer: &go_shopify.CustomerOrder{
            FirstName: o.GetCustomer().GetFirstName(),
            LastName:  o.GetCustomer().GetLastName(),
            Phone:     o.GetCustomer().GetPhone(),
            Email:     o.GetCustomer().GetEmail(),
            Status:    o.GetCustomer().GetStatus(),
        },
        BillingAddress: &go_shopify.Address{
            Name:      o.GetBillingAddress().GetName(),
            FirstName: o.GetBillingAddress().GetFirstName(),
            LastName:  o.GetBillingAddress().GetLastName(),
            Phone:     o.GetBillingAddress().GetPhone(),
            Address1:  o.GetBillingAddress().GetAddress1(),
            City:      o.GetBillingAddress().GetCity(),
            Country:   o.GetBillingAddress().GetCountry(),
            Zip:       o.GetBillingAddress().GetZip(),
        },
        ShippingAddress: &go_shopify.Address{
            Name:      o.GetShippingAddress().GetName(),
            FirstName: o.GetShippingAddress().GetFirstName(),
            LastName:  o.GetShippingAddress().GetLastName(),
            Phone:     o.GetShippingAddress().GetPhone(),
            Address1:  o.GetShippingAddress().GetAddress1(),
            City:      o.GetShippingAddress().GetCity(),
            Country:   o.GetShippingAddress().GetCountry(),
            Zip:       o.GetShippingAddress().GetZip(),
        },
        Transactions: buildTransactionToCreate(o.GetTransactions()),
    }
    return data
}

func buildOrderItemsToCreate(items []*cpb.OrderItem) []go_shopify.LineItem {
    if nil != items && 0 == len(items) {
        return nil
    }

    var lineItems []go_shopify.LineItem

    for _, item := range items {
        li := go_shopify.LineItem{
            ID:        util.StringToInt64(item.GetId()),
            VariantID: util.StringToInt64(item.GetProductId()),
            ProductID: util.StringToInt64(item.GetProductId()),
            Title:     item.GetName(),
            Name:      item.GetName(),
            SKU:       item.GetSku(),
            Price:     calculateItemPrice(item),
            Quantity:  int(item.GetQuantity()),
            TaxLines:  buildTaxLineToCreate(item.GetTaxLines()),
        }
        lineItems = append(lineItems, li)
    }

    return lineItems
}
func buildTransactionToCreate(r []*cpb.Transaction) []go_shopify.Transaction {
    if nil != r && 0 == len(r) {
        return nil
    }

    var transaction []go_shopify.Transaction
    for _, v := range r {
        payAmount, _ := decimal.NewFromString(v.Amount)
        locationID := util.StringToInt64(v.LocationId)
        tran := go_shopify.Transaction{
            ID:         util.StringToInt64(v.Id),
            OrderID:    util.StringToInt64(v.OrderId),
            Amount:     &payAmount,
            LocationID: &locationID,
            Kind:       v.Kind,
            Status:     v.Status,
        }
        transaction = append(transaction, tran)
    }
    return transaction
}

func calculateItemPrice(item *cpb.OrderItem) *decimal.Decimal {
    var price decimal.Decimal

    if util.IsValidNumber(item.GetPrice()) {
        price = util.StringToDecimal(item.GetPrice())
    }
    return &price
}

func buildTaxLineToCreate(taxLines []*tpb.TaxLine) []go_shopify.TaxLine {
    if nil == taxLines || 0 == len(taxLines) {
        return nil
    }

    var taxes []go_shopify.TaxLine

    for _, item := range taxLines {
        data := go_shopify.TaxLine{Title: item.Title}

        if !util.IsValidNumber(item.Price) && !util.IsValidNumber(item.Rate) {
            continue
        }

        price := util.StringToDecimal(item.Price).Round(2)
        rate := util.StringToDecimal(item.Rate)
        data.Price = &price
        data.Rate = &rate

        taxes = append(taxes, data)
    }

    return taxes
}
