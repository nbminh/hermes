package order

import (
    "fmt"
    "google.golang.org/protobuf/types/known/timestamppb"

    addpb "gitlab.com/minh2101/shopify_proto/address"
    cupb "gitlab.com/minh2101/shopify_proto/customer"
    cpb "gitlab.com/minh2101/shopify_proto/order"
    ppb "gitlab.com/minh2101/shopify_proto/payment"
    tpb "gitlab.com/minh2101/shopify_proto/tax"
    go_shopify "gitlab.com/nbminh/shopify_go"
)

func PrepareOrderTaxLines(items []go_shopify.TaxLine) []*tpb.TaxLine {
    if nil == items || len(items) == 0 {
        return nil
    }

    var taxLinesData []*tpb.TaxLine

    for _, item := range items {
        taxLine := &tpb.TaxLine{
            Price: fmt.Sprintf("%v", item.Price),
            Rate:  fmt.Sprintf("%v", item.Rate),
            Title: item.Title,
        }
        taxLinesData = append(taxLinesData, taxLine)
    }

    return taxLinesData
}

func PrepareOrderItem(items []go_shopify.LineItem) []*cpb.OrderItem {
    if nil == items || len(items) == 0 {
        return nil
    }

    var OrderItem []*cpb.OrderItem
    for _, v := range items {
        ordItem := &cpb.OrderItem{
            Id:        fmt.Sprintf("%d", v.ID),
            ProductId: fmt.Sprintf("%v", v.ProductID),
            Price:     fmt.Sprintf("%v", v.Price),
            Name:      v.Name,
            Taxable:   v.Taxable,
            Sku:       v.SKU,
            Quantity:  float32(v.Quantity),
            TaxLines:  PrepareOrderTaxLines(v.TaxLines),
        }
        OrderItem = append(OrderItem, ordItem)
    }
    return OrderItem
}
func PrepareOrderCustomer(items *go_shopify.CustomerOrder) *cupb.Customer {
    data := &cupb.Customer{
        Id:        fmt.Sprintf("%d", items.ID),
        FirstName: items.FirstName,
        LastName:  items.LastName,
        Email:     items.Email,
        Phone:     items.Phone,
        Status:    items.Status,
        CreatedAt: timestamppb.New(*items.CreatedAt),
        UpdatedAt: timestamppb.New(*items.UpdatedAt),
    }
    return data
}
func PrepareAddress(item *go_shopify.Address) *addpb.Address {
    return &addpb.Address{
        FirstName: item.FirstName,
        LastName:  item.LastName,
        Address1:  item.Address1,
        City:      item.City,
        Country:   item.Country,
        Name:      item.Name,
        Phone:     item.Phone,
        Zip:       item.Zip,
    }
}

func PrepareOrderTransactions(items []go_shopify.Transaction) []*cpb.Transaction {
    if nil == items || 0 == len(items) {
        return nil
    }
    transactions := make([]*cpb.Transaction, len(items))

    for idx, it := range items {
        transaction := &cpb.Transaction{
            Id:      fmt.Sprintf("%d", it.ID),
            OrderId: fmt.Sprintf("%d", it.OrderID),
            Kind:    it.Kind,
            Status:  it.Status,
        }

        if nil != it.LocationID {
            transaction.LocationId = fmt.Sprintf("%d", *it.LocationID)
        }
        if nil != it.Amount {
            transaction.Amount = it.Amount.String()
        }

        transactions[idx] = transaction
    }
    return transactions
}
func PrepareOrderRefund(item []go_shopify.Refund) []*cpb.Refund {
    if nil == item || 0 == len(item) {
        return nil
    }
    refunds := make([]*cpb.Refund, len(item))

    for i, v := range item {
        refund := &cpb.Refund{
            Id:           fmt.Sprintf("%v", v.ID),
            OrderId:      fmt.Sprintf("%v", v.OrderID),
            Transactions: PrepareOrderTransactions(v.Transactions),
            CreatedAt:    fmt.Sprintf("%v", v.CreatedAt),
            ProcessedAt:  fmt.Sprintf("%v", v.ProcessedAt),
        }
        refunds[i] = refund
    }
    return refunds
}

func PrepareResponsePayment(payment []go_shopify.Transaction) []*ppb.PaymentInfo {
    if nil == payment || 0 == len(payment) {
        return nil
    }
    var payments []*ppb.PaymentInfo

    for _, item := range payment {
        paymentIf := &ppb.PaymentInfo{
            Id:          uint64(item.ID),
            Amount:      item.Amount.String(),
            Currency:    item.Currency,
            PaymentCode: "manual",
            PaymentName: "Manual",
            Type:        item.Kind,
            Status:      item.Status,
            ProcessedAt: item.ProcessedAt.String(),
        }
        payments = append(payments, paymentIf)
    }

    return payments
}
