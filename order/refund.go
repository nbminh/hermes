package order

import (
    "strconv"

    "gitlab.com/nbminh/hermes/util"

    pb "gitlab.com/minh2101/shopify_proto/platform"
    go_shopify "gitlab.com/nbminh/shopify_go"
)

func prepareRefundToCreate(r *pb.RefundRequest, refundTransactions []go_shopify.Transaction) go_shopify.Refund {
    refundItems := prepareRefundItemsToCreate(r)

    refund := go_shopify.Refund{
        OrderID:         int(util.StringToInt64(r.GetId())),
        Shipping:        go_shopify.RefundShipping{},
        RefundLineItems: refundItems,
        Note:            r.GetNote(),
    }
    if nil != refundTransactions && len(refundTransactions) > 0 {
        for _, rt := range refundTransactions {
            if nil == rt.Amount {
                continue
            }
            refAmount := *rt.Amount
            refund.Transactions = append(refund.Transactions, go_shopify.Transaction{
                OrderID:  rt.OrderID,
                ParentID: rt.ParentID,
                Amount:   &refAmount,
                Kind:     "refund",
                Gateway:  rt.Gateway,
            })
        }
    }
    return refund
}

func prepareRefundItemsToCreate(r *pb.RefundRequest) []go_shopify.RefundLineItem {
    if nil == r.GetItems() || 0 == len(r.GetItems()) {
        return nil
    }
    var items []go_shopify.RefundLineItem
    intLocationId, _ := strconv.Atoi(r.GetLocationId())

    for _, item := range r.GetItems() {
        data := go_shopify.RefundLineItem{
            LineItemID: int(util.StringToInt64(item.Id)),
            Quantity:   int(item.Quantity),
            LocationID: intLocationId,
        }
        items = append(items, data)
    }

    return items
}
