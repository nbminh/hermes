package order

import (
    "context"

    "gitlab.com/nbminh/hermes/util"

    cpb "gitlab.com/minh2101/shopify_proto/order"
    pb "gitlab.com/minh2101/shopify_proto/platform"
    go_shopify "gitlab.com/nbminh/shopify_go"
)

type Service struct {
    app *go_shopify.App
}

func NewService(app *go_shopify.App) *Service {
    return &Service{app: app}
}

func (s *Service) Get(ctx context.Context, r *pb.OneOrderRequest) (*cpb.Order, error) {
    orderId := util.StringToInt64(r.GetId())
    order, err := s.app.NewClient().Order.Get(orderId, nil)
    if nil != err {
        return nil, err
    }
    response := prepareOrderToResponse(order)
    return response, err
}

func (s *Service) Refund(ctx context.Context, r *pb.RefundRequest) (*cpb.Order, error) {
    orderId := util.StringToInt64(r.GetId())

    res, _ := s.app.NewClient().Refund.Calculate(buildRefundEstimateToCreate(r), int(orderId))
    refundTransactions := make([]go_shopify.Transaction, 1)
    if nil != res {
        refundTransactions = res.Transactions
    }

    refundData := prepareRefundToCreate(r, refundTransactions)
    _, err := s.app.NewClient().Refund.Create(refundData, refundData.OrderID)

    if nil != err {
        return nil, err
    }
    return s.Get(ctx, &pb.OneOrderRequest{Id: r.GetId()})
}

func (s *Service) Create(ctx context.Context, r *pb.CUOrderRequest) (*cpb.Order, error) {
    orderData := buildOrderToCreate(r.GetOrder())
    order, err := s.app.NewClient().Order.Create(orderData)
    if nil != err {
        return nil, err
    }
    transactions, err := s.app.NewClient().Transaction.List(order.ID, nil)

    respone := prepareOrderToResponse(order)
    respone.Payments = PrepareResponsePayment(transactions)

    return respone, err
}
