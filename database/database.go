package database

import (
    "gorm.io/driver/mysql"
    "gorm.io/gorm"

    models "gitlab.com/nbminh/shopify_models"
)

var DB *gorm.DB

const (
    DB_USERNAME = "root"
    DB_PASSWORD = "my-secret-pw"
    DB_NAME     = "shopify"
    DB_HOST     = "localhost"
    DB_PORT     = "3307"
)

func ConnectDB() (*gorm.DB, error) {
    var err error
    dsn := DB_USERNAME + ":" + DB_PASSWORD + "@tcp" + "(" + DB_HOST + ":" + DB_PORT + ")/" + DB_NAME + "?" + "parseTime=true&loc=Local"

    database, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})

    if nil != err {
        return nil, err
    }

    database.AutoMigrate(&models.Product{})
    DB = database
    return DB, err
}
