package product

import (
    "gitlab.com/nbminh/hermes/database"
    go_shopify "gitlab.com/nbminh/shopify_go"
    models "gitlab.com/nbminh/shopify_models"
)

var DB, err = database.ConnectDB()

func CreateProductDB(items []go_shopify.Product) {
    if nil == err {
        var products []*models.Product
        DB.Select("product_id").Find(&products)

        listID := make(map[int64]int64)
        for _, v := range products {
            listID[v.ProductID] = v.ProductID
        }

        for _, v := range items {
            if _, status := listID[v.ID]; status {
                continue
            }

            if 0 == len(v.Variants) {
                product := models.Product{ProductID: v.ID, Name: v.Title, Type: v.ProductType, Status: v.Status}
                DB.Create(&product)
            } else {
                for j, _ := range v.Variants {
                    product := models.Product{ProductID: v.ID, Name: v.Title, Sku: v.Variants[j].Sku,
                        Price: v.Variants[j].Price, Type: v.ProductType, Status: v.Status}
                    DB.Create(&product)
                }
            }
        }
    }
}
