package cmd

import (
    "gitlab.com/nbminh/hermes/order"
    "log"
    "net"

    "github.com/spf13/cobra"
    "google.golang.org/grpc"

    pb "gitlab.com/minh2101/shopify_proto/platform"
    "gitlab.com/nbminh/hermes/product"
    "gitlab.com/nbminh/hermes/shopify"
    go_shopify "gitlab.com/nbminh/shopify_go"
)

var grpcCmd = &cobra.Command{
    Use:   "grpc",
    Short: "Start the gRPC server of Hermes",
    Run:   runGrpcCmd,
}

func runGrpcCmd(cmd *cobra.Command, args []string) {
    lis, err := net.Listen("tcp", ":9001")
    if err != nil {
        log.Fatalf("failed to listen: %v", err)
    }
    app := shopify.NewShopifyApp("redirectUrl")

    grpcServer := grpc.NewServer()
    grpcServer = initializeServices(grpcServer, app)

    if err = grpcServer.Serve(lis); err != nil {
        log.Fatalf("failed to serve: %s", err)
    }
}
func initializeServices(grpcServer *grpc.Server, app *go_shopify.App) *grpc.Server {
    productService := product.NewService(app)
    orderService := order.NewService(app)

    pb.RegisterProductServiceServer(grpcServer, productService)
    pb.RegisterOrderServiceServer(grpcServer, orderService)

    return grpcServer
}
